from glob import glob
from os.path import basename
from sklearn.model_selection import train_test_split 
import numpy as np
import random
import torch
from torch.utils import data
from os.path import join
from PIL import Image
import pandas as pd 
from torch import nn
from torchvision.models import squeezenet1_0
from torchvision import transforms
from torch.utils.data import DataLoader
from torch.optim import SGD
from sklearn.metrics import accuracy_score
from torch.utils.tensorboard import SummaryWriter

random.seed(51252)
np.random.seed(51252)

class CSVImageDataset(data.Dataset):
    def __init__(self, data_root, csv, transform=None):
        self.data_root=data_root
        self.data=pd.read_csv(csv)
        self.transform=transform
        
    def __len__(self):
        return len(self.data)
    
    def __getitem__(self, i):
        im_path, im_label = self.data.iloc[i]['path'],self.data.iloc[i].label
        im=Image.open(join(self.data_root,im_path)).convert('RGB')
        ctrans = transforms.CenterCrop(min(im.width, im.height))
        im = ctrans(im)
        
        if self.transform is not None:
            im=self.transform(im)
        return im, im_label
    

classes = pd.read_csv('classes.csv').to_dict()['class']
class_dict = {v: k for k, v in classes.items()}


path = "CustomTestSet"
if len(sys.argv) >= 2:
    path = sys.argv[1]
    
image_paths = glob(path + '/*/*')
image_paths=["/".join(p.split('/')[1:]) for p in image_paths]
def class_from_path(path):
    c,_ = path.split('/')
    return class_dict[c]
labels = [class_from_path(p) for p in image_paths]
dataset=pd.DataFrame({'path':image_paths,'label':labels})
dataset.to_csv('custom_test_set.csv', index=None)

test_transform=transforms.Compose([transforms.Resize(256),
                                   transforms.CenterCrop(224),
                                   transforms.ToTensor(),
                                   transforms.Normalize([0.485,0.456,0.406], [0.229,0.224,0.225])])

dataset_test = CSVImageDataset(path + '/','custom_test_set.csv', transform=test_transform)
ultimate_loader =DataLoader(dataset_test, batch_size=32, num_workers=2)
device = 'cuda' if torch.cuda.is_available() else 'cpu'


class AverageValueMeter():
  def __init__(self):
    self.reset()
    
  def reset(self):
    self.sum = 0
    self.num = 0
    
  def add(self, value, num):
    self.sum += value*num
    self.num += num
    
  def value(self):
    try:
      return self.sum/self.num
    except:
      return None


coin_model = squeezenet1_0(pretrained=False)
num_class = len(classes)
coin_model.classifier[1] = nn.Conv2d(512, num_class, kernel_size=(1,1), stride=(1,1))
coin_model.num_classes = num_class 

moload = torch.load("coin_model.pth", map_location=torch.device('cpu'))
coin_model.load_state_dict(moload["state_dict"])
coin_model.eval()

print("Model loaded (trained for ", moload["epoch"], ")")

def test_classifier(model, loader):
  device="cuda" if torch.cuda.is_available() else "cpu"
  model.to(device)
  predictions, labels=[], []
  for batch in loader:
    x=batch[0].to(device)
    y=batch[1].to(device)
    output=model(x)
    preds=output.to('cpu').max(1)[1].numpy()
    labs=y.to('cpu').numpy()
    predictions.extend(list(preds))
    labels.extend(list(labs))
  return np.array(predictions), np.array(labels)


predictions,labels = test_classifier(coin_model,ultimate_loader)
print("Accuracy: ", accuracy_score(labels,predictions)*100)

