from glob import glob
from os.path import basename
from sklearn.model_selection import train_test_split 
import numpy as np
import random
import torch
from torch.utils import data
from os.path import join
from PIL import Image
import pandas as pd 
from torch import nn
from torchvision.models import squeezenet1_0
from torchvision import transforms
from torch.utils.data import DataLoader
from torch.optim import SGD
from sklearn.metrics import accuracy_score
from torch.utils.tensorboard import SummaryWriter

API_KEY = "INSERT_YOUR_BOT_KEY_HERE"

def split_train_val_test(dataset, perc=[0.6,0.1,0.3]):
    train, testval = train_test_split(dataset, test_size=perc[1]+perc[2])
    val, test = train_test_split(testval, test_size=perc[2]/(perc[1]+perc[2]))
    return train, val, test

random.seed(51252)
np.random.seed(51252)

class CSVImageDataset(data.Dataset):
    def __init__(self, data_root, csv, transform=None):
        self.data_root=data_root
        self.data=pd.read_csv(csv)
        self.transform=transform
        
    def __len__(self):
        return len(self.data)
    
    def __getitem__(self, i):
        im_path, im_label = self.data.iloc[i]['path'],self.data.iloc[i].label
        im=Image.open(join(self.data_root,im_path)).convert('RGB')
        
        if self.transform is not None:
            im=self.transform(im)
        return im, im_label

classes = pd.read_csv('classes.csv').to_dict()['class']
#dataset_train = CSVImageDataset('FRAME/','train_set.csv')
#dataset_valid = CSVImageDataset('FRAME/','validation_set.csv')
#dataset_test = CSVImageDataset('FRAME/','test_set.csv')
device = 'cuda' if torch.cuda.is_available() else 'cpu'

def get_squeezenet_model():
    model = squeezenet1_0(pretrained=True)
    num_class = len(classes)
    model.classifier[1] = nn.Conv2d(512, num_class, kernel_size=(1,1), stride=(1,1))
    model.num_classes = num_class
    return model

train_transform=transforms.Compose([transforms.Resize(256),
                                    transforms.RandomCrop(224),
                                    transforms.RandomHorizontalFlip(),
                                    transforms.ToTensor(),
                                    transforms.Normalize([0.485,0.456,0.406], [0.229,0.224,0.225])])

test_transform=transforms.Compose([transforms.Resize(256),
                                   transforms.CenterCrop(224),
                                   transforms.ToTensor(),
                                   transforms.Normalize([0.485,0.456,0.406], [0.229,0.224,0.225])])


#coin_train=CSVImageDataset('FRAME/','train_set.csv', transform=train_transform)
#coin_valid=CSVImageDataset('FRAME/','validation_set.csv', transform=test_transform)
#coin_test =CSVImageDataset('FRAME/','test_set.csv', transform=test_transform)

#coin_train_loader=DataLoader(coin_train, batch_size=128, num_workers=2, shuffle=True)
#coin_valid_loader=DataLoader(coin_valid, batch_size=128, num_workers=2)
#coin_test_loader =DataLoader(coin_test, batch_size=128, num_workers=2)

class AverageValueMeter():
  def __init__(self):
    self.reset()
    
  def reset(self):
    self.sum = 0
    self.num = 0
    
  def add(self, value, num):
    self.sum += value*num
    self.num += num
    
  def value(self):
    try:
      return self.sum/self.num
    except:
      return None

def trainval_classifier(model, train_loader, valid_loader, exp_name='experiment', lr=0.01, epochs=10, momentum=0.99):
    criterion=nn.CrossEntropyLoss() 
    optimizer=SGD(model.parameters(), lr, momentum=momentum) 
    loss_meter=AverageValueMeter()
    acc_meter=AverageValueMeter()
    writer= SummaryWriter('log_tt')
    #loss_logger=VisdomPlotLogger('line', env=exp_name, opts={'title':'Loss','legend':['train','valid']})
    #acc_logger=VisdomPlotLogger('line', env=exp_name, opts={'title':'Accuracy','legend':['train','valid']})
    #visdom_saver=VisdomSaver(envs=[exp_name])
    device="cuda" if torch.cuda.is_available() else "cpu"
    model.to(device)
    loader={'train': train_loader,'valid': valid_loader}
    
    def save_checkpoint(model, epoch):
        torch.save({'state_dict': model.state_dict(),'epoch': epoch},"{}_{}.pth".format(exp_name,'checkpoint'))
        
    for e in range(epochs):
        print("Starting epoch ", e+1)
        for mode in ['train','valid'] :
            loss_meter.reset(); acc_meter.reset()
            model.train() if mode == 'train' else model.eval()
            with torch.set_grad_enabled(mode=='train'):
                for i, batch in enumerate(loader[mode]):
                    x=batch[0].to(device)
                    y=batch[1].to(device)
                    output=model(x)
                    l=criterion(output,y)
                    if mode=='train':
                        l.backward()
                        optimizer.step()
                        optimizer.zero_grad()
                        
                    acc=accuracy_score(y.to('cpu'),output.to('cpu').max(1)[1])
                    n=batch[0].shape[0]
                    loss_meter.add(l.item(),n)
                    acc_meter.add(acc,n)
                    print("Batch %s   %3d/%3d" % (mode,i+1,len(loader[mode])))
                    #if mode=='train':
                    #    loss_logger.log(e+(i+1)/len(loader[mode]), loss_meter.value()[0], name=mode)
                    #    acc_logger.log(e+(i+1)/len(loader[mode]), acc_meter.value()[0], name=mode)
            #loss_logger.log(e+1, loss_meter.value()[0], name=mode)
            #acc_logger.log(e+1, acc_meter.value()[0], name=mode)
            writer.add_scalar('loss/' + mode, loss_meter.value(), global_step=e)
            writer.add_scalar('acc/' + mode, acc_meter.value(), global_step=e)
        print("End of epoch ", e+1, ". Loss: ", loss_meter.value(), "  Accuracy: ", acc_meter.value())
        save_checkpoint(model, e)
    return model

coin_model = squeezenet1_0(pretrained=False)
num_class = len(classes)
coin_model.classifier[1] = nn.Conv2d(512, num_class, kernel_size=(1,1), stride=(1,1))
coin_model.num_classes = num_class 

moload = torch.load("coin_model.pth", map_location=torch.device('cpu'))
coin_model.load_state_dict(moload["state_dict"])
coin_model.eval()

print("Model loaded (trained for ", moload["epoch"], ")")

if False:
    timg =Image.open(sys.argv[i]).convert('RGB')
    timg = test_transform(timg)
    timg = timg.unsqueeze(0)

    clout = coin_model(timg)
    preconf = torch.nn.functional.softmax(clout[0], dim=0).tolist()
    ind = np.argmax(preconf)
    #tot = np.sum(clout.tolist()[0])
    
    confidences = [(i,preconf[i]) for i in range(len(classes))]
    #confidences = [(i,clout.tolist()[0][i]/tot) for i in range(len(classes))]
    confidences.sort(key = lambda x : x[1])
    
    print("Image ", sys.argv[i])
    print(classes[clout.max(1)[1].numpy()[0]])
    print("Class is ", classes[ind])
    for i in range(len(confidences)-1,-1,-1):
        print("%3s€ - %f%%" % (classes[confidences[i][0]],confidences[i][1]*100))
    print("\n")


import telebot

#const TeleBot = require('telebot');
bot = telebot.TeleBot(API_KEY);

index = 0
def photo_handler(messages):
    global index
    for m in messages:
        if m.content_type == 'photo':
            p = m.photo[0]
            plink = 'https://api.telegram.org/bot' + API_KEY + '/getFile?file_id=' + p.file_id
            file_info = bot.get_file(p.file_id)
            downloaded_file = bot.download_file(file_info.file_path)
            with open("botimages/image" + str(index) + ".jpg", 'wb') as new_file:
                new_file.write(downloaded_file)
                
            ctrans = transforms.CenterCrop(min(p.width, p.height))
            timg =Image.open("botimages/image" + str(index) + ".jpg").convert('RGB')
            if p.width != p.height:
                timg = ctrans(timg)
            timg = test_transform(timg)
            timg = timg.unsqueeze(0)
            clout = coin_model(timg)
            preconf = torch.nn.functional.softmax(clout[0], dim=0).tolist()
            ind = np.argmax(preconf)
            confidences = [(i,preconf[i]) for i in range(len(classes))]
            confidences.sort(key = lambda x : x[1])
            
            answer = "Class is " + str(classes[ind])
            for i in range(len(confidences)-1,-1,-1):
                answer = answer + "\n" + ("%3s€ - %f%%" % (classes[confidences[i][0]],confidences[i][1]*100))
            
            bot.reply_to(m, answer)
            index = index + 2

bot.set_update_listener(photo_handler)
bot.polling()

while True:
    pass
