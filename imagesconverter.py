from glob import glob
import os
import sys

path = "FRAME"

if len(sys.argv) >= 2:
    path = sys.argv[1]

image_paths = glob(path + '/*/*/*')

for s in image_paths:
    path = s[:-3] + "jpg"
    os.system("convert \"%s\" -resize 256x256 -quality 90 \"%s\"" % (s,path));
    
for s in image_paths:
    os.system("rm \"" + s + "\"")
    
